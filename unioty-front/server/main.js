import { Meteor } from 'meteor/meteor';
import {Mongo} from 'meteor/mongo';
MONGO_URL = "mongodb://mongo-unioty.default:27017/unioty";

Meteor.startup(() => {
  // code to run on server at startup
  let Iot = new Mongo.Collection('iots');
    let Tweet = new Mongo.Collection("tweets", {
  transform: function(tweet) {
    tweet.iot = Iot.findOne({
      _id: tweet.iot
    });
    return tweet;
  }
});
    Tweet.allow({
        insert: function () {
            return true;
        },
        update: function () {
            return true;
        },
        remove: function () {
            return true;
        }
    });

    Meteor.publish("tweets", function() {
        return Tweet.find({}, {sort: {created: -1}});
    });
});
