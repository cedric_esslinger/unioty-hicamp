'use strict';

/**
 * Module dependencies
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  iot = mongoose.model('Iot'),
  path = require('path'),
  config = require(path.resolve('./config/config')),
  chalk = require('chalk');

/**
 * tweet Schema
 */
var TweetSchema = new Schema({
  created: {
    type: Date,
    default: Date.now
  },
  severity: {
    type: String,
    default: '',
    trim: true
  },
  text: {
    type: String,
    default: '',
    trim: true,
    required: 'text cannot be blank'
  },
  userAction: {
    type: Boolean,
    default: '',
    trim: true
  },
  iot: {
    type: Schema.ObjectId,
    ref: 'Iot'
  }
});

mongoose.model('Tweet', TweetSchema);
