'use strict';

/**
 * Module dependencies
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Tweet = mongoose.model('Tweet'),
  Iot = mongoose.model('Iot'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * Create an article
 */
exports.create = function (req, res) {
  var tweet = new Tweet(req.body);
  // tweet.iot = req.iot;

  tweet.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
         //var result = tweet.text.match("(@+\\w*)");
         var result = tweet.text.match("(\\d{2})h(\\d{2})?");
         if(result.length > 2){
              var hours = result[1];
              var minutes = result[2];

              var clockAlarm = hours + ':' + minutes + ':00';
              if(minutes == undefined){
                   clockAlarm = hours + ':00:00';
              }

              var message = {
               topic: 'alarm_clock_cmd',
               payload: clockAlarm, // or a Buffer
               qos: 0, // 0, 1, or 2
               retain: false // or true
            };
            global.moscaServer.publish(message, function() {
               console.log('done!');
            });
         }
      res.setHeader('Access-Control-Allow-Origin', 'https://unioty.its-future.com');
      res.json(tweet);
    }
  });
};

/**
 * Show the current article
 */
exports.read = function (req, res) {
  // convert mongoose document to JSON
  var tweet = req.tweet ? req.tweet.toJSON() : {};

  res.json(tweet);
};

/**
 * Update an article
 */
// exports.update = function (req, res) {
//   var article = req.article;
//
//   article.title = req.body.title;
//   article.content = req.body.content;
//
//   article.save(function (err) {
//     if (err) {
//       return res.status(422).send({
//         message: errorHandler.getErrorMessage(err)
//       });
//     } else {
//       res.json(article);
//     }
//   });
// };

/**
 * Delete an article
 */
exports.delete = function (req, res) {
  var tweet = req.tweet;

  tweet.remove(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(tweet);
    }
  });
};

/**
 * List of Articles
 */
exports.list = function (req, res) {
  Tweet.find().sort('-created').populate("iot").exec(function (err, tweets) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.setHeader('Access-Control-Allow-Origin', 'https://unioty.its-future.com');
      res.json(tweets);
    }
  });
};

/**
 * Article middleware
 */
exports.tweetByID = function (req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Tweet is invalid'
    });
  }

  Tweet.findById(id).populate("iot").exec(function (err, tweet) {
    if (err) {
      return next(err);
    } else if (!tweet) {
      return res.status(404).send({
        message: 'No tweet with that identifier has been found'
      });
    }
    req.tweet = tweet;
    next();
  });
};

exports.createFromMQTT = function (messageObject){
     Iot.findOne({identifier: messageObject.objectId}).exec(function (err, iot) {
       if (err) {
         console.log("Add tweet error: " + err);
       } else if (!iot) {
         console.log("Add tweet error: object not found (" + messageObject.objectId + ")");
       }

       var myTweet = {};
       myTweet.text = messageObject.message;
       myTweet.severity = messageObject.severity;
       myTweet.userAction = messageObject.userAction;
       myTweet.iot = iot;

       var newTweet = new Tweet(myTweet);

       newTweet.save(function (err) {
         if (err) {
          console.log("Add tweet error: " + err);
         } else {
          console.log("Tweet SUCCESSFULLY CREATED");
         }
       });

     });
}
