'use strict';

/**
 * Module dependencies
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  path = require('path'),
  config = require(path.resolve('./config/config'));

/**
 * Article Schema
 */
var IotSchema = new Schema({
  identifier:{
       type: Number
 },
  type: {
    type: String,
    default: ''
  },
  name: {
    type: String,
    default: '',
    trim: true
  },
  displayName: {
    type: String,
    default: ''
  }
});

mongoose.model('Iot', IotSchema);
