'use strict';

/**
 * Module dependencies
 */
var iots = require('../controllers/iot.server.controller');

module.exports = function (app) {
  // Articles collection routes
  app.route('/api/iots')
    .get(iots.list)
    .post(iots.create);

  // Single article routes
  app.route('/api/iots/:iotId')
  .get(iots.read)
  .put(iots.update);

  app.route('/api/temperature')
  .post(iots.logTemperature);

  // Finish by binding the article middleware
  app.param('iotId', iots.iotByID);
};
