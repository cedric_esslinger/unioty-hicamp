'use strict';

var mosca = require('mosca');
var path = require('path');
var tweet = require('../../../unioty/server/controllers/tweet.server.controller');


module.exports = function (app) {
     var ascoltatore = {
       type: 'mongo',
       url: process.env.MONGO_URL || 'mongodb://localhost:27017/mqtt',
       pubsubCollection: 'ascoltatori',
       mongo: {}
     };

     var moscaSettings = {
       port: process.env.MQTT_PORT || 1883,
       backend: ascoltatore,
       persistence: {
         factory: mosca.persistence.Mongo,
         url: process.env.MONGO_URL || 'mongodb://localhost:27017/mqtt'
       }
     };

     console.log("MONGO: " + ascoltatore.url);

     global.moscaServer = new mosca.Server(moscaSettings);
     moscaServer.on('ready', setup);

     moscaServer.on('clientConnected', function(client) {
       console.log('client connected', client.id);
     });

     var test = true;

     global.moscaServer.on('published', function(packet, client) {
       console.log('Published', String(packet.payload));

       if(packet.topic == 'tweet'){
            var messageObject = JSON.parse(packet.payload);
           console.log("Message: " + messageObject.message);
           tweet.createFromMQTT(messageObject);
      }else if(packet.topic == 'alarm_clock'){
           var messageObject = JSON.parse(packet.payload);
           console.log("Message: " + messageObject.message);
           tweet.createFromMQTT(messageObject);
           if(messageObject.severity == 'INFO'){
                var message = {
                 topic: 'coffee_cmd',
                 payload: String('ON'), // or a Buffer
                 qos: 0, // 0, 1, or 2
                 retain: false // or true
              };
              global.moscaServer.publish(message, function() {
                 console.log('caffe demandé !');
              });
           }
      }else if(packet.topic == 'coffee'){
           var messageObject = JSON.parse(packet.payload);
           console.log("Message: " + messageObject.message);
           tweet.createFromMQTT(messageObject);
      }

       if(!test) {
         global.moscaServer.publish(message, client, function() {
           console.log('done!');
         });
         test = true;
       }
     });

     function setup() {
       console.log('Mosca server is up and running');
     }

     var message = {
       topic: 'subtopic',
       payload: 'HELLO', // or a Buffer
       qos: 0, // 0, 1, or 2
       retain: false // or true
     };

}
