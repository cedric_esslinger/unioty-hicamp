'use strict';

/**
 * Module dependencies
 */
var tweet = require('../controllers/tweet.server.controller');

module.exports = function (app) {
  // Articles collection routes
  app.route('/api/tweet')
    .get(tweet.list)
    .post(tweet.create);

  // Single article routes
  app.route('/api/tweet/:tweetId')
  //   .get(articles.read)
  //   .put(articles.update)
    .delete(tweet.delete);

  // Finish by binding the article middleware
  app.param('tweetId', tweet.tweetByID);
};
