'use strict';

/**
 * Module dependencies
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Iot = mongoose.model('Iot'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * Create an article
 */
exports.create = function (req, res) {
  var iot = new Iot(req.body);
  //article.user = req.user;

  iot.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(iot);
    }
  });
};

/**
 * Update an article
 */
exports.update = function (req, res) {
  var iot = req.iot;

  iot.name = req.body.name;
  iot.displayName = req.body.displayName;

  iot.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(iot);
    }
  });
};

/**
 * Show the current article
 */
exports.read = function (req, res) {
  // convert mongoose document to JSON
  var iot = req.iot ? req.iot.toJSON() : {};

  res.json(iot);
};

/**
 * List of Articles
 */
exports.list = function (req, res) {
  Iot.find().sort('-date').exec(function (err, iots) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(iots);
    }
  });
};

/**
 * Create an article
 */
exports.logTemperature = function (req, res) {
  console.log("TEMPERATURE: " + req.body.t + " - HUMIDITE: "+ req.body.h);
  res.status(200).send();
};

/**
 * Article middleware
 */
exports.iotByID = function (req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Iot is invalid'
    });
  }

  Iot.findById(id).exec(function (err, iot) {
    if (err) {
      return next(err);
} else if (!iot) {
      return res.status(404).send({
        message: 'No iot with that identifier has been found'
      });
    }
    req.iot = iot;
    next();
  });
};
