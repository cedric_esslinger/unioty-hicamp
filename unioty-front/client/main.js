import {Template} from 'meteor/templating';
import {Session} from 'meteor/session';
import {Mongo} from 'meteor/mongo';
import {HTTP} from 'meteor/http'
import './main.html';

let Iot = new Mongo.Collection('iots');

let Tweet = new Mongo.Collection('tweets',{
     transform: function(tweet) {
          tweet.iot = Iot.findOne({
               _id: tweet.iot
          });
          return tweet;
     }
});

let api_url = 'https://unioty-server.its-future.com';

tweetList = [];
Session.set('filterActive', 'false');
Session.set('filters', []);

Template.navbar.onCreated(function navbarOnCreated() {
     Meteor.subscribe('tweets');
});

// Meteor.setInterval(function () {
//     HTTP.call('Get', 'http://unioty-server.its-future.com/api/tweet/', (error, result) => {
//         this.tweetList = result;
//         if (!error) {
//             Session.set('twizzled', true);
//         } else {
//             console.log(error)
//         }
//     });
// }, 3000);

Template.navbar.events({
    'click .navbar-filter': function () {
        if (Session.get('filterActive') === "false") {
            Session.set('filterActive', "true");
        } else {
            Session.set('filterActive', "false");
        }
    }
});

Template.tweetFilter.events({
    'click .unioty-filter-close-action': function () {
        Session.set('filterActive', "false");
    },
    'change select': function (event) {
        Session.set("filters", "[]");
        for (var i = 0; i < event.target.options.length; i++) {
            if (event.target.options[i].selected) {
                var datas = JSON.parse(Session.get("filters"));
                datas.push(event.target.options[i].value);
                var stringdata = JSON.stringify(datas);
                Session.set("filters", stringdata);
            }
        }
        console.log(Session.get("filters"));
    }
});

Template.tweet.events({
    'click .unioty-like': function (event, template) {
        if (template.data.like) {
            Tweet.update({_id: template.data._id}, {$set: {like: false}});
        } else {
            Tweet.update({_id: template.data._id}, {$set: {like: true}});
        }
    },
    'click .unioty-unlike': function (event, template) {
        if (template.data.unlike) {
            Tweet.update({_id: template.data._id}, {$set: {unlike: false}});
        } else {
            Tweet.update({_id: template.data._id}, {$set: {unlike: true}});
        }
    }
});


Template.tweetFilter.helpers({
    'isFilterDisplay': function () {
        return Session.get('filterActive') === "true";
    }
});

Template.tweet.helpers({
    'isSub': function (sub) {
        return sub === "true";
    },
    'hasSub': function (parent) {
        return parent === "true";
    },
    'getURLOfType': function (type) {
        if (type === "ALARM_CLOCK") {
            return "images/timer.svg";
        } else if (type === "COFFEE_MACHINE") {
            return "images/coffee-maker.svg";
        } else {
            return "images/account.svg";
        }
    },
    'getSeverity': function (severity) {
        if (severity === "ERROR") {
            return "critical";
        } else if (severity === "WARN") {
            return "warning";
        } else if (severity === "ACQUITTED") {
            return "acquitted";
        } else {
            return "none";
        }
    },
    'getDisplayName': function (displayName) {
        return displayName ? displayName : "Moi";
    }
});

Template.registerHelper('formatDate', function (date) {
    return moment(date).format('DD/MM/YYYY');
});
Template.registerHelper('diffDate', function (date) {
    var now = new Date();
    var created = new Date(date);
    var diff = moment.utc(moment(now).diff(moment(created)));
    if (diff.date() > 1) {
        return "Il y a " + diff.format("D") + "j";
    }
    if (diff.hour() > 0) {
        return "Il y a " + diff.format("H") + "h";
    }
    if (diff.minute() > 0) {
        return "Il y a " + diff.format("m") + "min";
    }
    if (diff.second() > 0) {
        return "Il y a " + diff.format("s") + "s";
    }
    if (diff.second() === 0) {
        return "Maintenant";
    }
    return diff.format('HH:mm:ss');
});

Template.body.helpers({
    getTweets() {
        // return this.tweetList;
        if (Session.get("filters").length > 0) {
            var datas = JSON.parse(Session.get("filters"));
            return Tweet.find({severity: {$in: datas}}, {sort: {created: -1}}).fetch();
        } else {
            return Tweet.find({}, {sort: {created: -1}}).fetch();
        }
    }
});

Template.footer.events({
    'submit .new-tweet'(event) {
        // Prevent default browser form submit
        event.preventDefault();

        // Get value from form element
        const target = event.target;
        let text = null;
        let userAction = null;
        let object = null;
        let severity = null;
        let date = null;
        if (target) {
            if (target.text) {
                text = target.text.value;
                if (target.userAction) {
                    userAction = target.userAction.value;
                }
                if (target.severity) {
                    severity = target.severity.value;
                } else {
                    severity = "INFO";
                }
                if (target.object) {
                    object = target.object.value;
                }
                if (target.date) {
                    date = target.date.value;
                }

                let obj = {
                    text: text,
                    userAction: userAction,
                    date: date,
                    severity: severity,
                    object: object
                };

                HTTP.call('POST', api_url + '/api/tweet/', {data: {params: obj}}, (error, result) => {
                    if (!error) {
                        Session.set('twizzled', true);
                    } else {
                        console.log(error)
                    }
                });

                // Clear form
                target.text.value = '';
            }
        }
    }
});
